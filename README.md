# doubanP7
模拟一个豆瓣app。未完成。
## 项目目标
1. 使用原始react完成基本功能、使用react-router和context技术
2. 使用ts进行重构
3. 使用state管理技术，比如mobx、redux等技术

**感谢[豆瓣](http://api.douban.com)提供的api支持**